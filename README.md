# xiomi-notifier

![Demonstration](.gitlab/demo.gif)

The android app component of https://gitlab.com/vitineth/xiomi-dispatcher. This recieves
notifications via Firebase and logs them into a Room database, displaying them in a list in the app
and allowing you trace through notifications linked with the same correlation ID. 