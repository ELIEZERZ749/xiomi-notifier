package org.xiomi.notifier

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.CheckCircle
import androidx.compose.material.icons.rounded.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Alignment.Companion.Top
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat
import org.xiomi.notifier.activities.EventActivity
import org.xiomi.notifier.activities.SettingsActivity
import org.xiomi.notifier.data.*
import org.xiomi.notifier.data.entities.Event
import org.xiomi.notifier.firebase.XiomiFirebaseMessagingService
import org.xiomi.notifier.ui.theme.XiomiNotifierTheme
import org.xiomi.notifier.utils.TimeSince
import org.xiomi.notifier.utils.createNotificationChannel
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.util.*

class MainActivity : ComponentActivity() {

    val database by lazy { NotifierDatabase.getDatabase(this) }

    private fun configure() {
        XiomiFirebaseMessagingService.configure(this)
    }

    // Declare the launcher at the top of your Activity/Fragment:
    private val requestPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted: Boolean ->
        if (isGranted) {
            // FCM SDK (and your app) can post notifications.
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.INTERNET
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                if (Build.VERSION.SDK_INT >= 33) {
                    if (ContextCompat.checkSelfPermission(
                            this,
                            Manifest.permission.POST_NOTIFICATIONS
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        configure()
                    }
                } else {
                    configure()
                }
            }
        } else {
            // TODO: Inform user that that your app will not show notifications.
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Cannot function without permission")
            builder.setMessage("This app exclusively delivers and tracks notifications delivered through Firebase which requires this permission - without it, the app cannot function. To make the app function, please go to your settings and enable the permission")
            builder.setPositiveButton("Ok") { dialog, _ ->
                dialog.dismiss()
            }
            builder.show()
        }
    }

    private fun askRequiredPermission(permission: String, onSuccess: (() -> Unit)? = null) {
        if (ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED
        ) {
            // FCM SDK (and your app) can post notifications.
            onSuccess?.invoke()
        } else if (shouldShowRequestPermissionRationale(permission)) {
            // TODO: display an educational UI explaining to the user the features that will be enabled
            //       by them granting the POST_NOTIFICATION permission. This UI should provide the user
            //       "OK" and "No thanks" buttons. If the user selects "OK," directly request the permission.
            //       If the user selects "No thanks," allow the user to continue without notifications.
            // TODO: Inform user that that your app will not show notifications.
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Cannot function without permission")
            builder.setMessage("This app exclusively delivers and tracks notifications delivered through Firebase which requires this permission - without it, the app cannot function. Do you want to retry?")
            builder.setPositiveButton("Ok") { dialog, _ ->
                dialog.dismiss()
                askRequiredPermission(permission, onSuccess)
            }
            builder.setNegativeButton("Cancel") { dialog, _ ->
                dialog.dismiss()
            }
            builder.show()
        } else {
            // Directly ask for the permission
            requestPermissionLauncher.launch(permission)
        }
    }

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("MainActivity", "Launching")
        if (Build.VERSION.SDK_INT >= 33) askRequiredPermission(Manifest.permission.POST_NOTIFICATIONS){
            Log.d("MainActivity", "Configuring")
            configure()
        }else{
            configure()
        }
        askRequiredPermission(Manifest.permission.INTERNET)

        createNotificationChannel(
            this@MainActivity,
            R.string.default_notification_channel_id,
            R.string.default_notification_channel_name,
            R.string.default_notification_channel_description
        )

        actionBar?.hide()

        setContent {
            XiomiNotifierTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Scaffold(
                        topBar = {
                            CenterAlignedTopAppBar(
                                title = { Text("Notifications") },
                                actions = {
                                    IconButton(onClick = {
                                        val intent = Intent(this@MainActivity, SettingsActivity::class.java)
                                        this@MainActivity.startActivity(intent)
                                    }) {
                                        Icon(
                                            Icons.Rounded.Settings,
                                            contentDescription = "Settings"
                                        )
                                    }
                                }
                            )
                        },
                        content = { innerPadding ->
                            val state =
                                database.eventDao().getAll().collectAsState(initial = listOf())

                            Box(Modifier.padding(innerPadding)) {
                                FailStateEventList(
                                    events = state.value.sortedBy { it.timestamp }
                                        .reversed(),
                                    context = this@MainActivity,
                                    modifier = Modifier.padding(10.dp)
                                )
                            }
                        }
                    )
//                    Greeting("Android")
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Composable
fun FailStateEventList(
    events: List<Event>,
    modifier: Modifier = Modifier,
    context: Context? = null
) {
    if (events.isEmpty()) {
        Column(
            Modifier
                .fillMaxWidth()
                .padding(top = 50.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Icon(
                Icons.Outlined.CheckCircle,
                contentDescription = "No notifications",
                Modifier
                    .size(70.dp)
                    .padding(bottom = 10.dp),
                tint = Color.LightGray
            )
            Text("No notifications", color = Color.LightGray)
        }
    } else {
        EventList(events, modifier, context)
    }
}

@Composable
fun EventList(events: List<Event>, modifier: Modifier = Modifier, context: Context? = null) {
    Log.d("Main", "${events.size} events")
    LazyColumn(modifier = modifier) {
        items(events) { item ->
            AlertCard(event = item, context = context)
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AlertCard(event: Event, context: Context? = null) {

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 10.dp),
        onClick = {
            context?.let {
                val intent = Intent(it, EventActivity::class.java)
                intent.putExtra("event_id", event.uid.toString())
                context.startActivity(intent)
            }
            Log.d("Main", "clicked!")
        }
    ) {
        Box(
            Modifier
                .fillMaxWidth()
                .padding(Dp(7F))
        ) {
            Column {
                Row {
                    Box(
                        Modifier
                            .clip(CircleShape)
                            .size(35.dp)
                            .align(Top)
                            .background(Color.White)
                    ) {
                        Icon(
                            eventStateToIcon(event.status),
                            contentDescription = "Message Notification",
                            Modifier.align(
                                Center
                            ),
                            tint = eventStateToColor(event.status),
                        )
                    }
                    Column(
                        modifier = Modifier
                            .padding(start = 7.dp)
                            .align(CenterVertically),
                    ) {
                        Text(
                            text = "${event.module}: ${event.summary}",
                            fontWeight = FontWeight.Bold
                        )
                        TimeSince(
                            time = event.timestamp,
                            fontStyle = FontStyle.Italic,
                            fontSize = 8.sp
                        )
                    }
                }
                Column(
                    Modifier.padding(7.dp)
                ) {

                    Text(
                        event.detail ?: "No description provided",
                        fontSize = 10.sp,
                        lineHeight = 15.sp
                    )
                    event.properties?.let {
                        Column(modifier = Modifier.padding(start = 10.dp)) {
                            Spacer(modifier = Modifier.height(5.dp))
                            it.entries.map { entry ->
                                Row {
                                    Text(
                                        entry.key,
                                        fontSize = 8.sp
                                    )
                                    Text(
                                        "=",
                                        fontSize = 8.sp,
                                        modifier = Modifier.padding(end = 6.dp, start = 6.dp)
                                    )
                                    Text(
                                        entry.value,
                                        fontFamily = FontFamily.Monospace,
                                        fontSize = 8.sp
                                    )
                                }
                            }
                        }
                    }
                    Row(
                        Modifier.padding(top = 5.dp)
                    ) {
                        Text(
                            "Topic",
                            fontSize = 10.sp,
                            fontWeight = FontWeight.Bold,
                            modifier = Modifier.padding(end = 3.dp)
                        )
                        Text(event.topic, fontSize = 10.sp)
                        Spacer(Modifier.width(10.dp))
                        Text(
                            "Correlation",
                            fontSize = 10.sp,
                            fontWeight = FontWeight.Bold,
                            modifier = Modifier.padding(end = 3.dp)
                        )
                        Text(event.correlationID?.toString() ?: "uncorrelated", fontSize = 10.sp)
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    val entries = listOf(
        Event(
            UUID.randomUUID(),
            UUID.randomUUID(),
            EventState.CRITICAL,
            "module",
            "topic",
            "This is some short summary",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
            null,
            ZonedDateTime.now(ZoneOffset.UTC)
        ),
        Event(
            UUID.randomUUID(),
            UUID.randomUUID(),
            EventState.MESSAGE,
            "module",
            "topic",
            "This is some short summary",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
            mapOf(
                "a" to "b",
                "something" to "else",
                "example" to "values"
            ),
            ZonedDateTime.now(ZoneOffset.UTC)
        ),
    )
    XiomiNotifierTheme {
        // A surface container using the 'background' color from the theme
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            FailStateEventList(events = listOf<Event>().sortedBy { it.timestamp })
        }
    }
}