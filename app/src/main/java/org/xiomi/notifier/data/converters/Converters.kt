package org.xiomi.notifier.data.converters

import androidx.room.TypeConverter
import org.json.JSONObject
import org.xiomi.notifier.data.EventState
import java.time.Instant
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.util.*

class Converters {

    @TypeConverter
    fun fromTimestamp(value: Long?): ZonedDateTime? {
        return value?.let {
            ZonedDateTime.ofInstant(Instant.ofEpochSecond(it), ZoneOffset.UTC)
        }
    }

    @TypeConverter
    fun toTimestamp(value: ZonedDateTime?): Long? {
        return value?.toEpochSecond()
    }

    @TypeConverter
    fun fromUUID(value: UUID?): String? {
        return value?.toString()
    }

    @TypeConverter
    fun toUUID(value: String?): UUID? {
        return value?.let {
            return UUID.fromString(it)
        }
    }

    @TypeConverter
    fun fromEventState(value: EventState?): String? {
        return value?.name
    }

    @TypeConverter
    fun toEventState(value: String?): EventState? {
        return value?.let {
            EventState.valueOf(it)
        }
    }

    @TypeConverter
    fun fromStringMap(value: Map<String, String>?): String? {
        return value?.let {
            val json = JSONObject()
            it.keys.forEach { k -> json.put(k, it[k]) }
            return@let json.toString()
        }
    }

    @TypeConverter
    fun toStringMap(value: String?): Map<String, String>? {
        return value?.let { input ->
            val json = JSONObject(input)
            return@let json.keys().asSequence().associateBy({ it }, { json.getString(it) })
        }
    }

}