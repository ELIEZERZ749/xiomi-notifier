package org.xiomi.notifier.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.xiomi.notifier.data.EventState
import java.time.ZonedDateTime
import java.util.*

@Entity
data class Event(
    @PrimaryKey val uid: UUID,
    @ColumnInfo(name = "correlation_id") val correlationID: UUID?,
    @ColumnInfo(name = "status") val status: EventState,
    @ColumnInfo(name = "module") val module: String,
    @ColumnInfo(name = "topic") val topic: String,
    @ColumnInfo(name = "summary") val summary: String,
    @ColumnInfo(name = "detail") val detail: String?,
    @ColumnInfo(name = "properties") val properties: Map<String, String>?,
    @ColumnInfo(name = "timestamp") val timestamp: ZonedDateTime,
)