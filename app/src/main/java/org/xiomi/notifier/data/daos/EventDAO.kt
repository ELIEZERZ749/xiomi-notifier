package org.xiomi.notifier.data.daos

import androidx.room.*
import kotlinx.coroutines.flow.Flow
import org.xiomi.notifier.data.entities.Event
import java.util.*

@Dao
interface EventDAO {

    @Query("SELECT * FROM event")
    fun getAll(): Flow<List<Event>>

    @Query("SELECT * FROM event WHERE timestamp >= datetime('now', '-24 Hour')")
    fun getLast24Hours(): Flow<List<Event>>

    @Query("SELECT * FROM event WHERE uid=:id LIMIT 1")
    fun getByID(id: String): Flow<Event?>

    @Query("SELECT * FROM event WHERE correlation_id=:id")
    fun getByCorrelation(id: UUID): Flow<List<Event>>?

    @Insert
    fun insertAll(vararg events: Event)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertAll(vararg event: Event)

    @Delete
    fun delete(event: Event):Int

}