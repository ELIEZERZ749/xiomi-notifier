package org.xiomi.notifier.data

import kotlinx.coroutines.flow.Flow
import org.xiomi.notifier.data.daos.EventDAO
import org.xiomi.notifier.data.entities.Event

class EventRepository(private val eventDAO: EventDAO) {

    val getAll: Flow<List<Event>> = eventDAO.getAll();

}