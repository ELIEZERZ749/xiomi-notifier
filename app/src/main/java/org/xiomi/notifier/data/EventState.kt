package org.xiomi.notifier.data

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.*
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector

enum class EventState {
    NOTIFY,
    ALERT,
    CRITICAL,
    RESOLVING,
    RESOLVED,
    UNKNOWN,
    MESSAGE,
}

fun eventStateToColor(state: EventState): Color {
    return when (state) {
        EventState.MESSAGE -> Color.hsl(211F, 0.14F, 0.40F)
        EventState.ALERT -> Color.hsl(41F, 0.86F, 0.72F)
        EventState.CRITICAL -> Color.hsl(0F, 0.72F, 0.65F)
        EventState.NOTIFY -> Color.hsl(187F, 0.63F, 0.53F)
        EventState.RESOLVED -> Color.hsl(165F, 0.83F, 0.37F)
        EventState.RESOLVING -> Color.hsl(205F, 0.87F, 0.61F)
        EventState.UNKNOWN -> Color.hsl(250F, 0.32F, 0.43F)
    }
}

fun eventStateToIcon(state: EventState): ImageVector {
    return when (state) {
        EventState.MESSAGE -> Icons.Rounded.Email
        EventState.ALERT -> Icons.Rounded.Notifications
        EventState.CRITICAL -> Icons.Rounded.Warning
        EventState.NOTIFY -> Icons.Rounded.Info
        EventState.RESOLVED -> Icons.Rounded.Check
        EventState.RESOLVING -> Icons.Rounded.Done
        EventState.UNKNOWN -> Icons.Rounded.Build
    }

}