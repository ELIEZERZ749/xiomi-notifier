package org.xiomi.notifier.activities

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.shrinkVertically
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.rounded.Delete
import androidx.compose.material.icons.rounded.KeyboardArrowLeft
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import org.xiomi.notifier.EventList
import org.xiomi.notifier.R
import org.xiomi.notifier.data.*
import org.xiomi.notifier.data.entities.Event
import org.xiomi.notifier.ui.theme.XiomiNotifierTheme
import org.xiomi.notifier.utils.TimeSince
import org.xiomi.notifier.utils.createNotificationChannel
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.util.*
import java.util.concurrent.Executors

class EventActivity : ComponentActivity() {

    val database by lazy { NotifierDatabase.getDatabase(this) }

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        actionBar?.hide()
        super.onCreate(savedInstanceState)

        createNotificationChannel(
            this@EventActivity,
            R.string.default_notification_channel_id,
            R.string.default_notification_channel_name,
            R.string.default_notification_channel_description
        )

        if (intent.extras == null) {
            finish()
            return
        }
        if (!intent.hasExtra("event_id")) {
            finish()
            return
        }
        val eventID = intent.getStringExtra("event_id")!!

        setContent {
            val state = database.eventDao().getByID(eventID).collectAsState(initial = null)
            state.value?.let { event ->
                Content(
                    event,
                    onDelete = {
                        Executors.newSingleThreadExecutor().execute {
                            database.eventDao().delete(event)
                            Log.d("DELETE", "Deleting event: " + event.uid.toString())
                        }
                    },
                    event.correlationID?.let {
                        database.eventDao().getByCorrelation(event.correlationID)
                    },
                    this@EventActivity
                )
            } ?: Box {

            }
        }
    }

}

fun showDeleteConfirm(context: Context, onDelete: () -> Unit) {
    val builder = android.app.AlertDialog.Builder(context)
    builder.setTitle("Are you sure?")
    builder.setMessage("You cannot undo this action")
    builder.setPositiveButton("Delete") { dialog, _ ->
        onDelete()
        dialog.dismiss()
    }
    builder.setNegativeButton("Cancel") { dialog, _ ->
        dialog.dismiss()
    }
    builder.show()
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Content(
    event: Event,
    onDelete: () -> Unit,
    correlated: Flow<List<Event>>?,
    context: Activity? = null
) {
    XiomiNotifierTheme {
        // A surface container using the 'background' color from the theme
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.background
        ) {
            Scaffold(
                topBar = {
                    SmallTopAppBar(
                        title = { Text("View") },
                        navigationIcon = {
                            IconButton(onClick = {
                                context?.finish()
                            }) {
                                Icon(
                                    Icons.Rounded.KeyboardArrowLeft,
                                    contentDescription = "Back"
                                )
                            }
                        },
                        actions = {
                            IconButton(onClick = {
                                context?.let {
                                    showDeleteConfirm(
                                        it
                                    ) {
                                        onDelete()
                                        it.finish()
                                    }
                                }
                            }) {
                                Icon(
                                    Icons.Rounded.Delete,
                                    contentDescription = "Delete"
                                )
                            }
                        }
                    )
                },
                content = { innerPadding ->
                    Box(
                        Modifier
                            .padding(innerPadding)
//                            .fillMaxSize()
                    ) {
                        Column(
                            Modifier.padding(10.dp)
                        ) {
                            Card(
                                Modifier
                                    .fillMaxWidth()
                                    .wrapContentHeight()
                            ) {
                                Column {
                                    Row {
                                        Box(
                                            Modifier
                                                .clip(
                                                    SemiShape()
                                                )
                                                .height(70.dp)
                                                .width(60.dp)
                                                .background(eventStateToColor(event.status))
                                                .align(CenterVertically)
                                        ) {
                                            Layout(
                                                content = {
                                                    Icon(
                                                        eventStateToIcon(event.status),
                                                        contentDescription = "Icon",
                                                        modifier = Modifier.align(Alignment.Center),
                                                        tint = Color.White
                                                    )
                                                },
                                                measurePolicy = { measurables, constraints ->
                                                    val icon = measurables[0].measure(constraints)
                                                    layout(
                                                        constraints.maxWidth / 2,
                                                        constraints.maxHeight
                                                    ) {
                                                        icon.placeRelative(
                                                            IntOffset(
                                                                -icon.width / 2,
                                                                (constraints.maxHeight / 2) - (icon.height / 2)
                                                            )
                                                        )
                                                    }
                                                }
                                            )
                                        }
                                        Column(
                                            Modifier
                                                .padding(top = 5.dp, bottom = 5.dp)
                                                .offset(x = (-20).dp)
                                        ) {
                                            Text(
                                                text = "${event.module}: ${event.summary}",
                                                fontWeight = FontWeight.Bold
                                            )
                                            TimeSince(
                                                time = event.timestamp,
                                                fontStyle = FontStyle.Italic,
                                                fontSize = 8.sp
                                            )
                                            Text(
                                                event.detail ?: "No description provided",
                                                fontSize = 10.sp,
                                                lineHeight = 15.sp
                                            )
                                            Row(
                                                Modifier.padding(7.dp)
                                            ) {
                                                Text(
                                                    "Topic",
                                                    fontSize = 10.sp,
                                                    fontWeight = FontWeight.Bold,
                                                    modifier = Modifier.padding(end = 3.dp)
                                                )
                                                Text(event.topic, fontSize = 10.sp)
                                                Spacer(Modifier.width(10.dp))
                                                Text(
                                                    "Correlation",
                                                    fontSize = 10.sp,
                                                    fontWeight = FontWeight.Bold,
                                                    modifier = Modifier.padding(end = 3.dp)
                                                )
                                                Text(
                                                    event.correlationID?.toString()
                                                        ?: "uncorrelated",
                                                    fontSize = 10.sp
                                                )
                                            }
                                        }
                                    }
                                }
                            }
                            Column(
//                                Modifier.fillMaxSize(),
                                verticalArrangement = Arrangement.SpaceBetween
                            ) {
                                Column(
                                    Modifier.padding(5.dp)
                                ) {
                                    event.properties?.let {
                                        if (it.isEmpty()) return@let
                                        var state by remember { mutableStateOf(false) }

                                        Column(Modifier.padding(bottom = 10.dp)) {
                                            Row(
                                                Modifier.clickable {
                                                    state = !state
                                                }
                                            ) {
                                                Icon(
                                                    Icons.Default.KeyboardArrowDown,
                                                    contentDescription = "Expand"
                                                )
                                                Text(
                                                    "Additional Properties"
                                                )
                                            }

                                            val density = LocalDensity.current
                                            AnimatedVisibility(
                                                visible = state,
                                                enter = slideInVertically {
                                                    with(density) { -40.dp.roundToPx() }
                                                },
                                                exit = slideOutVertically() + shrinkVertically()
                                            ) {
                                                Box(
                                                    Modifier.padding(10.dp)
                                                ) {
                                                    Column {
                                                        it.map { entry ->
                                                            Row {
                                                                Text(
                                                                    entry.key,
                                                                    modifier = Modifier.fillMaxWidth(
                                                                        0.5f
                                                                    ),
                                                                    fontFamily = FontFamily.Monospace
                                                                )
                                                                Text(
                                                                    entry.value,
                                                                    modifier = Modifier.fillMaxWidth(
                                                                        0.5f
                                                                    ),
                                                                    fontFamily = FontFamily.Monospace
                                                                )
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    correlated?.let {
                                        Text(
                                            "Correlated Messages",
                                            fontWeight = FontWeight.Bold
                                        )

                                        val state = correlated.collectAsState(initial = listOf())
                                        EventList(events = state.value.filter { ev ->
                                            ev.uid != event.uid
                                        }.sortedBy { it.timestamp }, context = context)
                                    }
                                }

                            }
                        }
                    }
                },
                modifier = Modifier.fillMaxSize(),
                bottomBar = {
                    Row {
                        Button(
                            modifier = Modifier
                                .padding(4.dp)
                                .fillMaxWidth(0.5f),
                            onClick = { /*TODO*/ }
                        ) {
                            Text("View Module")
                        }
                        Button(
                            modifier = Modifier
                                .padding(4.dp)
                                .fillMaxWidth(),
                            onClick = { /*TODO*/ }
                        ) {
                            Text("View Topic")
                        }
                    }
                }
            )
        }
    }
}

class SemiShape : Shape {
    override fun createOutline(
        size: Size,
        layoutDirection: LayoutDirection,
        density: Density
    ): Outline {
        return Outline.Generic(path = semiPath(size))
    }
}

fun semiPath(size: Size): Path {
    return Path().apply {
        reset()

        moveTo(0f, 0f)
        arcTo(
            rect = Rect(
                left = -size.width / 2f,
                top = 0f,
                right = size.width / 2,
                bottom = size.height
            ),
            startAngleDegrees = -90f,
            sweepAngleDegrees = 180f,
            forceMoveTo = false
        )
        lineTo(0f, 0f)
        close()
    }
}

@Composable
@Preview
fun Preview() {
    Content(
        Event(
            UUID.randomUUID(),
            UUID.randomUUID(),
            EventState.MESSAGE,
            "module",
            "topic",
            "This is some short summary",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
            mapOf(
                "a" to "b",
                "something" to "else",
                "example" to "values"
            ),
            ZonedDateTime.now(ZoneOffset.UTC)
        ),
        onDelete = {

        },
        listOf<List<Event>>(
            listOf(
                Event(
                    UUID.randomUUID(),
                    UUID.randomUUID(),
                    EventState.MESSAGE,
                    "module",
                    "topic",
                    "This is some short summary",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
                    mapOf(
                        "a" to "b",
                        "something" to "else",
                        "example" to "values"
                    ),
                    ZonedDateTime.now(ZoneOffset.UTC)
                )
            )
        ).asFlow()
    )
}