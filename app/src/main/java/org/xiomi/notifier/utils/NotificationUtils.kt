package org.xiomi.notifier.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE


fun createNotificationChannel(
    context: Context,
    channelId: Int,
    channelName: Int,
    channelDescription: Int
) {
    // Create the NotificationChannel
    val name = context.getString(channelName)
    val descriptionText = context.getString(channelDescription)
    val importance = NotificationManager.IMPORTANCE_DEFAULT
    val mChannel = NotificationChannel(context.getString(channelId), name, importance)
    mChannel.description = descriptionText
    val notificationManager = context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
    notificationManager.createNotificationChannel(mChannel)
}