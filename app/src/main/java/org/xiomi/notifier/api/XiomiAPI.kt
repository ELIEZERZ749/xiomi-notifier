package org.xiomi.notifier.api

import android.content.Context
import android.util.Log
import androidx.preference.PreferenceManager
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.firebase.installations.FirebaseInstallations
import org.json.JSONObject
import java.nio.charset.StandardCharsets


class XiomiAPI {

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: RequestQueue? = null

        fun getQueue(context: Context): RequestQueue {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Volley.newRequestQueue(context)
                INSTANCE = instance
                // return instance
                instance
            }
        }

        fun sendRegistrationKey(context: Context, identifier: String) {
            val url = PreferenceManager.getDefaultSharedPreferences(context)
                .getString("checkin_url", "https://notifier.xiomi.org/checkin")
            Log.d("SendRegistration", "Sending registration for $identifier to $url")

            val body = JSONObject()
            body.put("identifier", identifier)

            val request = object : StringRequest(Request.Method.POST, url, {
                Log.d("SendRegistration", "Sent registration successfully")
            }, {
                Log.e("SendRegistration", "Failed to send request", it)
            }) {
                override fun getBodyContentType(): String {
                    return "application/json; charset=utf-8"
                }

                override fun getBody(): ByteArray {
                    return body.toString().toByteArray(StandardCharsets.UTF_8)
                }
            }

            getQueue(context).add(request)
        }
    }

}